* Proof of concept for updatable NS-records in caching name servers
  (see [Statess DNS Echo Server](https://gitlab.com/holst/StatelessDNS-EchoServer))
* Needs small patch of Perl
  [Net::DNS](http://search.cpan.org/~nlnetlabs/Net-DNS-1.04/lib/Net/DNS.pm)
  library to handle DNS Update [queries](https://www.ietf.org/rfc/rfc2136.txt)
