#!/usr/bin/perl

use strict;
use warnings;
use Net::DNS::Nameserver;
use Time::HiRes qw(sleep);

# Autoflush after every line
$| = 1;

#--------------------------------------------------------------------
# Config
#--------------------------------------------------------------------
my $ttl = 3600; # Default ttl
my $name_server = '127.0.0.1';
my $echo_domain = 'echo.localhost'; # Echo domain
# my $host_port = '5353';
my $host_port = '53';
# my $name_server = '134.34.3.3';
# my $echo_domain = 'echo3.netfuture.ch'; # Echo domain

#--------------------------------------------------------------------
# Argument handling
#--------------------------------------------------------------------
my $host_ip = "127.0.0.3";
my $host_dom = "_tcp.$echo_domain";
if (@ARGV) {
  if ($#ARGV != 1) {
    print("Take default values or call it with propagation 'ip domain'\n")
  }
  else {
    ($host_ip, $host_dom) = @ARGV;
    $host_dom = lc($host_dom).".$echo_domain";
  }
}
print ("Config: $host_ip/$host_dom\n");

# The client database (Intiliazed with own ip. Get only ask, if ns reqest is successful)
my %clients;
# $clients{$host_dom}{$host_ip} = 1;
# push(@{$clients{$host_dom}}, $host_ip);

sub add_rr {
  my ($sec_ref, $rr_str) = @_;

  print (" : $rr_str\n");
  my $rr = new Net::DNS::RR($rr_str);
  push(@$sec_ref, $rr);
}

sub add_ns {
  my ($sec_ns, $sec_add, $dom) = @_;

  foreach my $ip (keys %{$clients{$dom}}) {
    my $ns = "$ip.00.$dom";
    add_rr($sec_ns, "$dom $ttl IN NS $ns");
    add_rr($sec_add, "$ns $ttl IN A $ip");
  }
}

sub print_client {
  print ("Clients:\n");
  foreach my $dom (keys %clients) {
    print (" $dom:\n");
    foreach my $ip (keys %{$clients{$dom}}) {
      print ("   $ip\n");
    }
  }
}

sub reply_handler {
  my ($qname, $qclass, $qtype, $peerhost,$query,$conn) = @_;    # parameters
  my ($rcode, @ans, @auth, @add, %headermask);                  # return values

  # to lower case
  $qname = lc($qname);

  # Print question
  print("Q: $qname $qclass $qtype\n");


  if ($qtype eq 'A') {
    # NR
    if ($qname =~ /^(.*)\.(nar|00)\.(.*)$/) {
      my ($ip, $method, $domain) = ($1, $2, $3);
      if ($method eq 'nar') {
        if ($ip eq $host_ip) {
          # with own ip as data return random ip (ttl=0)
          add_rr(\@ans, "$qname 0 IN A 1.2.3.4");

          # Add own ip to clients list
          $clients{$host_dom}{$host_ip} = 1;
          print ("Added own ip $host_ip\n");
        }
        else {
          # Just the default answer
          add_rr(\@ans, "$qname 0 IN A 1.2.3.4");

          # New potential nameserver enters the hood
          # TODO Don't add when refusing
          $clients{$domain}{$ip} = 1;
          # TODO Send directly?
          print_client;
        }
      }
      elsif ($method eq '00') {
        # Every direct 00-method request deletes the ip (ttl=0)
        add_rr(\@ans, "$qname 0 IN A $ip");

        delete ($clients{$domain}{$ip});
        # # TODO Update the other ns
      }
      else {
        print('Something terrible happend!!\n');
      }
    }
    else {
      # Publish actual nameserver list through A record (ttl=0)
      # Could be a random ip, but to inform which ns answers...
      add_rr(\@ans, "$qname 0 IN A $host_ip");

      add_ns(\@auth, \@add, $host_dom);
    }
  }
  elsif ($qtype eq 'NS') {
    # Publish actual nameserver list
    add_ns(\@ans, \@add, $host_dom);
  }
  elsif ($qtype eq 'SOA') {
    add_rr(\@ans, "$qname IN SOA $qname root.$qname (1 2 3 4 5)");
  }
  print ("\n");

  #--------------------------------------------------------------------
  # return reply
  #--------------------------------------------------------------------
  $rcode  = "NOERROR";
  $headermask{aa} = 1; # mark the answer as authority by setting the 'aa' flag
  return ($rcode, \@ans, \@auth, \@add, \%headermask);
}

sub update_handler {
  my ($qname, $qclass, $qtype, $peerhost,$query,$conn) = @_;    # parameters
  my ($rcode, @ans, @auth, @add, %headermask);                  # return values
  $rcode = "NOERROR";

  # Ignore prerequisites

  # Only support one update request
  my $ucount = $query->header->upcount;
  if ($ucount > 1) {
    print ("Multiple update ($ucount) currently not supported");
    $rcode = 'FORMERR';
  } else {
    my ($update) = $query->update;
    my $uname = $update->name;
    my $utype = $update->type;
    my $uclass = $update->class;
    my $udata = $update->rdstring;

    print ("U: $uname $utype $uclass $udata\n");


    if ($uclass eq 'ANY') {
      # Update RRsets not supported
      $rcode = 'NOTIMP';
    }
    else {
      my $add = 1;
      if ($uclass eq 'NONE') {
        $add = 0;
      }

      if ($utype eq 'NS') {
        if ($uname =~ /^(.*)\.(000?)\.(.*)$/) {
          my ($ip, $method, $domain) = ($1, $2, $3);
          if ($add) {
            $clients{$domain}{$ip} = 1;
            print ("   Add $domain:$ip\n");
          }
          else {
            delete ($clients{$domain}{$ip});
            print ("   Delete $domain:$ip\n");
          }

          # TODO Add this for other rr-types
          if ($method eq '00') {
            # 00 is original request, 000 an already forwarded one
            foreach my $cip (keys %{$clients{$domain}}) {
              if ($cip ne $host_ip && $cip ne $ip) {
                print ("   Forward update to $cip\n");

                my $update = new Net::DNS::Update($host_dom);
		my $rr;
		if ($add) {
		    $rr = new Net::DNS::RR("$ip.000.$host_dom 3600 NS $host_dom");
		} else {
		    $rr = new Net::DNS::RR("$ip.000.$host_dom NONE 0 NS $host_dom")
		}
                $update->push( update => $rr);

                my $resolve_update = new Net::DNS::Resolver(
                  nameservers => [$cip]
                );
                my $reply = $resolve_update->send($update);
                # TODO handle reply (Not required in the prototype)
              }
            }

            # Update nameserver
          }
        }
      }


    }
  }


  print ("\n");
  #--------------------------------------------------------------------
  # return reply
  #--------------------------------------------------------------------
  return ($rcode, \@ans, \@auth, \@add, \%headermask);
}

sub async_resolve {
  my ($query) = @_;

  # fork
  my $pid = fork;
  if ($pid) { # parent
      return $pid;
  }
  # return if $pid;

  sleep (0.2);
  my $resolver = new Net::DNS::Resolver(
    nameservers => [$name_server],
    recursive => 0,
    debug => 0
  );
  # $resolver->cdflag(1);
  print("Async: $query\n");
  $resolver->send($query);
  exit; # ByeBye child
}


#--------------------------------------------------------------------
# Get a host_domain nameserver list
#--------------------------------------------------------------------
# Initilize a resolver
my $resolver = new Net::DNS::Resolver(
  nameservers => [$name_server],
  recursive => 0,
  debug => 0
);
# $resolver->cdflag(1);

my $packet = $resolver->query("$host_dom", 'NS');
if ($packet) {
  foreach my $rr ($packet->answer) {
    if ($rr->rdstring =~ /^(.*)\.00\..*$/) {
      my $ip = $1;
      $clients{$host_dom}{$ip} = 1;
    }
  }
}
print_client;

#--------------------------------------------------------------------
# Cleapup
#--------------------------------------------------------------------
sub exit_handler {
    my $ns_ip;
    foreach my $cip (keys(%{$clients{$host_dom}})) {
	if ($cip ne $host_ip) {
	    $ns_ip = $cip;
	    last;
	}
    }
    if ($ns_ip) {
	print ("Send delete request to '$ns_ip'\n");

	my $update = new Net::DNS::Update($host_dom);
	my $rr = new Net::DNS::RR("$host_ip.00.$host_dom NONE 0 NS $host_dom");
	$update->push( update => $rr);

	my $resolve_update = new Net::DNS::Resolver(
	    nameservers => [$ns_ip]
	    );
	my $reply = $resolve_update->send($update);

	my $pid = async_resolve($host_dom);
	waitpid ($pid, 0);
    } else {
	    print ("I'm the last one!! .... TODO\n");
    }

    # TODO
    die ("ByeBye\n");
}
$SIG{TERM} = \&exit_handler;
$SIG{INT} = \&exit_handler;

#--------------------------------------------------------------------
# Start the nameserver
#--------------------------------------------------------------------
my $ns = new Net::DNS::Nameserver(
  LocalAddr    => $host_ip,
  LocalPort    => $host_port,
  ReplyHandler => \&reply_handler,
  UpdateHandler => \&update_handler,
  Verbose	     => 0
) || die "couldn't create nameserver object\n";

# Popularize client
print ("Startup:\n");
if (!%clients) {
  # The first client?
  my $query = "$host_ip.nar.$host_dom";
  print (" Initialize query: $query\n");
  $resolver->bgsend($query);
}
elsif (exists ($clients{$host_dom}{$host_ip})) {
  print (" Already registered\n");
  # nothing else to do
}
else {
  # Send update
  my ($ns_ip) = keys(%{$clients{$host_dom}});
  print (" Send update to $ns_ip\n");

  my $update = new Net::DNS::Update($host_dom);
  my $rr = new Net::DNS::RR("$host_ip.00.$host_dom 3600 NS $host_dom");
  $update->push( update => $rr);

  my $resolve_update = new Net::DNS::Resolver(
    nameservers => [$ns_ip]
  );
  my $reply = $resolve_update->send($update);
  if ($reply) {
    if ($reply->header->rcode eq 'NOERROR') {
      print (" Update succeeded. Add own to ns...\n");
      $clients{$host_dom}{$host_ip} = 1;
      

      # # Send cache update queries
      # print ("Send nar..");
      # $resolver->bgsend("$host_ip.nar.$host_dom");
      # sleep (0.5);
      # print ("Send first dom");
      # $resolver->bgsend("$host_dom");
      # sleep (0.5);
      # print ("Send second dom");
      # $resolver->bgsend("$host_dom");

    }
    else {
      print (" Update failed with rcode ".$reply->header->rcode."\n");
    }
  }
  else {
    print (" Update failed: wörk,wörk,...\n");
  }
}

async_resolve($host_dom);

# and go....
$ns->main_loop;
